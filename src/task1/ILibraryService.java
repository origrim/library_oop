package task1;

public interface ILibraryService {
    void addBook(Library library, Book book);
    void deleteBook (Library library, Book book);
    Book findBooks (Library library, String title, String author, String year_of_publishing);
}
