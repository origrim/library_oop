package task1;

public class DemoService implements IDemoService {


    public void execute(){

        Library library = new Library();
        ILibraryService iLibraryService = new LibraryService();

        System.out.println("Добавление 5 книг");

        iLibraryService.addBook(library,new Book("Язык программирования C++", "Страуструп, Б.И.","2012г"));
        iLibraryService.addBook(library,new Book("Язык программирования Java и среда NetBeans", " Монахов, В.В.","2012г"));
        iLibraryService.addBook(library,new Book("Язык программирования С", " Керниган, Б.У.","2013г"));
        iLibraryService.addBook(library,new Book("Языки программирования. Концепции и принципы", " Кауфман, В.Ш.","2011г"));
        iLibraryService.addBook(library,new Book("Современные языки и технологии паралелльного программирования", " Гергель, В.П.","2012г"));

        System.out.println("Добавление 6 книги");

        iLibraryService.addBook(library,new Book("Государь", " Никколо Макиавелли","2017г"));

        System.out.println("Удаление книги");

        iLibraryService.deleteBook(library,new Book("Язык программирования С", " Керниган, Б.У.","2013г"));

        System.out.println("Поиск книги");

        iLibraryService.findBooks(library, "Язык программирования C++", "Страуструп, Б.И.","2012г");


    }

}
