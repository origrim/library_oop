package task1;

public class LibraryService implements ILibraryService {
    
    @Override
    public void addBook (Library library, Book book){

        for (int i = 0; i < library.getBooks().length ; i++) {

            if(library.getBooks()[i] == null){
                library.getBooks()[i] = book;
                return;
            }
        }
    }

    @Override
    public void deleteBook(Library library, Book book) {
        for (int i = 0; i < library.getBooks().length; i++) {
            if (library.getBooks()[i] == null) {
                continue;
            }
            if (library.getBooks()[i].equals(book)) {
                library.getBooks()[i] = null;}

        }
    }

    @Override
    public Book findBooks(Library library, String title, String author, String year_of_publishing) {
        Book book = new Book(title, author, year_of_publishing);
        for (int i = 0; i < library.getBooks().length; i++){
            if(library.getBookByIndex(i).equals(book)){
                return library.getBookByIndex(i);
            }
        }
        return null;
    }
}
