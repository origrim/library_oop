package task1;
import java.util.Arrays;
public class Library {

    private Book [] books = new Book[5];

    public Book[] getBooks() {
        return books;
    }
    public Book getBookByIndex(int index) {
        return books[index];
    }
}
