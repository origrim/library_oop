package task1;

import java.util.Objects;

public class Book {

    private String title;
    private String author;
    private String year_of_publishing;

   public Book (String title, String author, String year_of_publishing){
        this.title = title;
        this.author = author;
        this.year_of_publishing = year_of_publishing;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getYear_of_publishing() {
        return year_of_publishing;
    }

    public void setYear_of_publishing(String year_of_publishing) {
        this.year_of_publishing = year_of_publishing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return Objects.equals(getTitle(), book.getTitle()) &&
                Objects.equals(getAuthor(), book.getAuthor()) &&
                Objects.equals(getYear_of_publishing(), book.getYear_of_publishing());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getAuthor(), getYear_of_publishing());
    }
}
